import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';

import { useStyles } from './ContactStyles';
import { CONTACT_INITIAL_STATE, VALIDATION_SCHEMA } from './ContactConstants';
import ArrowButton from '../ui/arrow-button/ArrowButton';

import phoneIcon from '../../assets/phone.svg';
import emailIcon from '../../assets/email.svg';
import airplane from '../../assets/send.svg';

import { onSetValue } from '../../redux/header/header.actions';

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
});

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const Contact: React.FC<Props> = ({ setValue }) => {
  const nameRegEx = /^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/;
  const emailRegEx = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
  const phoneRegEx = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
  const textRegEx = /[^A-Za-z0-9 .'?!,@$#-_]/;

  const classes = useStyles();
  const theme = useTheme();
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'));
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'));
  const matchesXS = useMediaQuery(theme.breakpoints.down('xs'));

  const [contact, setContact] = useState(CONTACT_INITIAL_STATE);
  const [errorMessage, setErrorMessage] = useState(null);
  // const [name, setName] = useState<string>('');
  const [nameHelper, setNameHelper] = useState<string>('');
  // const [email, setEmail] = useState<string>('');
  const [emailHelper, setEmailHelper] = useState<string>('');
  // const [phone, setPhone] = useState<string>('');
  const [phoneHelper, setPhoneHelper] = useState<string>('');
  // const [message, setMessage] = useState<string>('');
  const [messageHelper, setMessageHelper] = useState<string>('');
  const [open, setOpen] = useState<boolean>(false);

  const onChange = (e: any) => {
    let isValid;
    switch (e.target.name) {
      case 'name':
        // setName(e.target.value);
        setContact({ ...contact, name: e.target.value });
        isValid = nameRegEx.test(e.target.value);
        if (!isValid) {
          setNameHelper('Invalid name');
        } else {
          setNameHelper('');
        }
        break;
      case 'email':
        // setEmail(e.target.value);
        setContact({ ...contact, email: e.target.value });
        isValid = emailRegEx.test(e.target.value);
        if (!isValid) {
          setEmailHelper('Invalid email');
        } else {
          setEmailHelper('');
        }
        break;
      case 'phone':
        // setPhone(e.target.value);
        setContact({ ...contact, phone: e.target.value });
        isValid = phoneRegEx.test(e.target.value);
        if (!isValid) {
          setPhoneHelper('Invalid phone');
        } else {
          setPhoneHelper('');
        }
        break;
      case 'message':
        // setMessage(e.target.value);
        setContact({ ...contact, message: e.target.value });
        isValid = textRegEx.test(e.target.value);
        if (!isValid) {
          setMessageHelper('Invalid message');
        } else {
          setMessageHelper('');
        }
      default:
        break;
    }
  };

  const isValid = () => {
    try {
      VALIDATION_SCHEMA.validateSync(contact, { abortEarly: true });
      return true;
    } catch (err) {
      if (err && err.hasOwnProperty('message')) {
        setErrorMessage(err.message);
      }
      return false;
    }
  };

  return (
    <Grid container direction="row">
      <Grid
        item
        container
        direction="column"
        alignItems="center"
        justify="center"
        style={{
          marginBottom: matchesMD ? '5em' : 0,
          marginTop: matchesSM ? '1em' : matchesMD ? '5em' : 0,
        }}
        lg={4}
        xl={3}
      >
        <Grid item>
          <Grid item container direction="column">
            <Grid item>
              <Typography
                variant="h2"
                align={matchesMD ? 'center' : undefined}
                style={{ lineHeight: 1 }}
              >
                Contact Us
              </Typography>
              <Typography
                variant="body1"
                align={matchesMD ? 'center' : undefined}
                style={{ color: theme.palette.primary.main }}
              >
                We're waiting.
              </Typography>
            </Grid>
            <Grid item container style={{ marginTop: '2em' }}>
              <Grid item>
                <img
                  src={phoneIcon}
                  alt="phone"
                  style={{ marginRight: '0.5em' }}
                />
              </Grid>
              <Grid item>
                <Typography
                  variant="body1"
                  style={{ color: theme.palette.primary.main, fontSize: '1em' }}
                >
                  <a
                    href="tel:555555555"
                    style={{ textDecoration: 'none', color: 'inherit' }}
                  >
                    (555) 555-5555
                  </a>
                </Typography>
              </Grid>
            </Grid>
            <Grid item container style={{ marginBottom: '2em' }}>
              <Grid item>
                <img
                  src={emailIcon}
                  alt="envelop"
                  style={{ marginRight: '0.5em', verticalAlign: 'bottom' }}
                />
              </Grid>
              <Grid item>
                <Typography
                  variant="body1"
                  style={{ color: theme.palette.primary.main, fontSize: '1em' }}
                >
                  <a
                    href="mailto:habib@gmail.com"
                    style={{ textDecoration: 'none', color: 'inherit' }}
                  >
                    habib@gmail.com
                  </a>
                </Typography>
              </Grid>
            </Grid>
            <Grid
              item
              container
              direction="column"
              style={{ maxWidth: '20em' }}
            >
              <Grid item>
                <div className={classes.error}>
                  &nbsp;{errorMessage && <span>{errorMessage}</span>}
                </div>
              </Grid>
              <Grid item style={{ marginBottom: '0.5em' }}>
                <TextField
                  label="Name"
                  id="name"
                  name="name"
                  error={nameHelper.length !== 0}
                  helperText={nameHelper}
                  fullWidth
                  value={contact.name}
                  onChange={onChange}
                />
              </Grid>
              <Grid item style={{ marginBottom: '0.5em' }}>
                <TextField
                  label="Email"
                  id="email"
                  name="email"
                  error={emailHelper.length !== 0}
                  helperText={emailHelper}
                  fullWidth
                  value={contact.email}
                  onChange={onChange}
                />
              </Grid>
              <Grid item style={{ marginBottom: '0.5em' }}>
                <TextField
                  label="Phone"
                  id="phone"
                  name="phone"
                  error={phoneHelper.length !== 0}
                  helperText={phoneHelper}
                  fullWidth
                  value={contact.phone}
                  onChange={onChange}
                />
              </Grid>
            </Grid>
            <Grid item style={{ maxWidth: '20em' }}>
              <TextField
                InputProps={{ disableUnderline: true }}
                id="message"
                name="message"
                error={messageHelper.length !== 0}
                helperText={messageHelper}
                fullWidth
                placeholder="Enter your message"
                multiline
                rows={5}
                value={contact.message}
                onChange={onChange}
                className={classes.message}
              />
            </Grid>
            <Grid item container justify="center" style={{ marginTop: '2em' }}>
              <Button
                // disabled={
                //   name.length === 0 ||
                //   email.length === 0 ||
                //   phone.length === 0 ||
                //   message.length === 0 ||
                //   emailHelper.length !== 0 ||
                //   phoneHelper.length !== 0
                // }
                variant="contained"
                className={classes.sendButton}
                onClick={() => setOpen(true)}
              >
                Sent Message
                <img
                  src={airplane}
                  alt="paper airplane"
                  style={{ marginLeft: '1em' }}
                />
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        fullScreen={matchesXS}
        style={{ zIndex: 1302 }}
        onClose={() => setOpen(false)}
        PaperProps={{
          style: {
            paddingTop: matchesXS ? '1em' : '5em',
            paddingBottom: matchesXS ? '1em' : '5em',
            paddingRight: matchesXS
              ? 0
              : matchesSM
              ? '5em'
              : matchesMD
              ? '10em'
              : '20em',
            paddingLeft: matchesXS
              ? 0
              : matchesSM
              ? '5em'
              : matchesMD
              ? '10em'
              : '20em',
          },
        }}
      >
        <DialogContent>
          <Grid container direction="column">
            <Grid item>
              <Typography variant="h4" align="center" gutterBottom>
                Confirm Message
              </Typography>
            </Grid>
            <Grid item style={{ marginBottom: '0.5em' }}>
              <TextField
                label="Name"
                id="name"
                name="name"
                error={nameHelper.length !== 0}
                helperText={nameHelper}
                fullWidth
                value={contact.name}
                onChange={onChange}
              />
            </Grid>
            <Grid item style={{ marginBottom: '0.5em' }}>
              <TextField
                label="Email"
                id="email"
                name="email"
                error={emailHelper.length !== 0}
                helperText={emailHelper}
                fullWidth
                value={contact.email}
                onChange={onChange}
              />
            </Grid>
            <Grid item style={{ marginBottom: '0.5em' }}>
              <TextField
                label="Phone"
                id="phone"
                name="phone"
                error={phoneHelper.length !== 0}
                helperText={phoneHelper}
                fullWidth
                value={contact.phone}
                onChange={onChange}
              />
            </Grid>
            <Grid item style={{ maxWidth: matchesXS ? '100%' : '20em' }}>
              <TextField
                InputProps={{ disableUnderline: true }}
                id="message"
                name="message"
                error={messageHelper.length !== 0}
                helperText={messageHelper}
                fullWidth
                placeholder="Enter your message"
                multiline
                rows={5}
                value={contact.message}
                onChange={onChange}
                className={classes.message}
              />
            </Grid>
            <Grid
              item
              container
              direction={matchesSM ? 'column' : 'row'}
              alignItems="center"
              style={{ marginTop: '2em' }}
            >
              <Grid item>
                <Button
                  color="primary"
                  style={{ fontWeight: 300 }}
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </Button>
              </Grid>
              <Grid item>
                <Button
                  // disabled={
                  //   name.length === 0 ||
                  //   email.length === 0 ||
                  //   phone.length === 0 ||
                  //   message.length === 0 ||
                  //   emailHelper.length !== 0 ||
                  //   phoneHelper.length !== 0
                  // }
                  variant="contained"
                  className={classes.sendButton}
                  onClick={() => setOpen(true)}
                >
                  Sent Message
                  <img
                    src={airplane}
                    alt="paper airplane"
                    style={{ marginLeft: '1em' }}
                  />
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
      <Grid
        item
        container
        direction={matchesMD ? 'column' : 'row'}
        alignItems="center"
        justify={matchesMD ? 'center' : undefined}
        className={classes.background}
        lg={8}
        xl={9}
      >
        <Grid
          item
          style={{
            marginLeft: matchesMD ? 0 : '3em',
            textAlign: matchesMD ? 'center' : 'inherit',
          }}
        >
          <Grid container direction="column">
            <Grid item>
              <Typography variant="h2" align={matchesMD ? 'center' : undefined}>
                Simple Software.
                <br />
                Revolutionary
              </Typography>
              <Typography
                variant="subtitle2"
                align={matchesMD ? 'center' : undefined}
                style={{ fontSize: '1.5rem' }}
              >
                Take advantage of the 21t Century
              </Typography>
              <Grid item container justify={matchesMD ? 'center' : undefined}>
                <Button
                  variant="outlined"
                  component={Link}
                  to="/revolution"
                  onClick={() => setValue(2)}
                  className={classes.learnMoreButton}
                >
                  <span style={{ marginRight: 5 }}>Learn More</span>
                  <ArrowButton
                    width={10}
                    height={10}
                    fill={theme.palette.primary.main}
                  />
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            component={Link}
            to="/estimate"
            onClick={() => setValue(5)}
            className={classes.estimateButton}
          >
            Free Estimate
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default connector(Contact);
