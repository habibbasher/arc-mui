import React from 'react';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { useStyles } from './ProcessItemStyles';

const ProcessItem: React.FC<any> = ({
  bgColor,
  headerText,
  icon,
  iconStyle,
  altText,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'));

  return (
    <Grid
      item
      container
      direction={matchesMD ? 'column' : 'row'}
      className={classes.rowContainer}
      style={{ backgroundColor: bgColor, height: '90em' }}
    >
      <Grid
        item
        container
        direction="column"
        alignItems={matchesMD ? 'center' : undefined}
        lg
      >
        <Grid item>
          <Typography
            variant="h4"
            align={matchesMD ? 'center' : undefined}
            gutterBottom
            style={{ color: '#000', marginTop: '5em' }}
          >
            {headerText}
          </Typography>
        </Grid>
        <Grid item>
          <Typography
            variant="body1"
            align={matchesMD ? 'center' : undefined}
            paragraph
            style={{ color: '#fff', maxWidth: '20em' }}
          >
            We see a future where every individuals has personalized software
            custom tailored to their lifestyle, culture, and interests, helping
            them overcome life's obstacles. Each project is a step towards that
            goal.
          </Typography>
          <Typography
            variant="body1"
            align={matchesMD ? 'center' : undefined}
            paragraph
            style={{ color: '#fff', maxWidth: '20em' }}
          >
            By holding ourselves to rigorous standards and pristine quality, we
            can ensure you have the absolute best tools necessary to thrive in
            this new frontier.
          </Typography>
          <Typography
            variant="body1"
            align={matchesMD ? 'center' : undefined}
            paragraph
            style={{ color: '#fff', maxWidth: '20em' }}
          >
            We see a future where every individuals has personalized software
            custom tailored to their lifestyle, culture, and interests, helping
            them overcome life's obstacles. Each project is a step towards that
            goal.
          </Typography>
        </Grid>
      </Grid>
      <Grid item lg style={{ alignSelf: 'center' }}>
        <img src={icon} alt={altText} width="100%" style={iconStyle} />
      </Grid>
    </Grid>
  );
};

export default ProcessItem;
