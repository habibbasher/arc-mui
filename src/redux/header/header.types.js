export const HeaderActionTypes = {
  SET_VALUE: 'SET_VALUE',
  SET_SELECTED_ITEM: 'SET_SELECTED_ITEM',
};
