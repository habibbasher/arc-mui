import React from 'react';
import Lottie from 'react-lottie';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import { useStyles } from './RevolutionStyles';

import vision from '../../assets/vision.svg';
import consultation from '../../assets/consultationIcon.svg';
import mockup from '../../assets/mockupIcon.svg';
import review from '../../assets/reviewIcon.svg';
import design from '../../assets/designIcon.svg';
import build from '../../assets/buildIcon.svg';
import launch from '../../assets/launchIcon.svg';
import maintain from '../../assets/maintainIcon.svg';
import iterate from '../../assets/iterateIcon.svg';
import technologyAnimation from '../../animations/technologyAnimation/data.json';
import ProcessItem from '../../components/process-item/ProcessItem';

const Revolution: React.FC = () => {
  const classes = useStyles();
  const theme = useTheme();
  const matchesMD = useMediaQuery(theme.breakpoints.down('md'));
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'));
  const matchesXS = useMediaQuery(theme.breakpoints.down('xs'));

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: technologyAnimation,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  return (
    <Grid container direction="column">
      <Grid item className={classes.rowContainer} style={{ marginTop: '2em' }}>
        <Typography
          variant="h2"
          align={matchesMD ? 'center' : undefined}
          style={{ fontFamily: 'Pacifico' }}
        >
          Revolution
        </Typography>
      </Grid>
      <Grid
        item
        container
        direction={matchesMD ? 'column' : 'row'}
        alignItems="center"
        className={classes.rowContainer}
        style={{ marginTop: '5em' }}
      >
        <Grid item lg>
          <img
            src={vision}
            alt="mountain through binoculars"
            style={{
              maxWidth: matchesSM ? 300 : '40em',
              marginRight: matchesMD ? 0 : '5em',
              marginBottom: matchesMD ? '5em' : 0,
            }}
          />
        </Grid>
        <Grid item container direction="column" lg style={{ maxWidth: '40em' }}>
          <Grid item>
            <Typography
              variant="h4"
              align={matchesMD ? 'center' : 'right'}
              gutterBottom
            >
              Vision
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : 'right'}
              paragraph
            >
              The rise of computers, and subsequently the Internet, has
              completely altered every aspect of human life. This has increased
              our comfort, broadened our connections, and reshaped how we view
              the world.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : 'right'}
              paragraph
            >
              What once was confined to huge rooms and teams of engineers now
              resides in every single one of our hands. Harnessing this
              unlimited potential by using it to solve problems and better lives
              is at the heart of everything we do.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : 'right'}
              paragraph
            >
              We want to help businesses capitalized on the latest and greatest
              technology. The best way to predict the future is to be the one
              building if, and we want to help guide the world into this next
              chapter of technological expansion, exploration, and innovation.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : 'right'}
              paragraph
            >
              By holding ourselves to rigorous standards and pristine quality,
              we can ensure you have the absolute best tools necessary to thrive
              in this new frontier.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : 'right'}
              paragraph
            >
              We see a future where every individuals has personalized software
              custom tailored to their lifestyle, culture, and interests,
              helping them overcome life's obstacles. Each project is a step
              towards that goal.
            </Typography>
          </Grid>
        </Grid>
      </Grid>
      <Grid
        item
        container
        direction={matchesMD ? 'column' : 'row'}
        alignItems="center"
        className={classes.rowContainer}
        style={{ marginTop: '10em', marginBottom: '10em' }}
      >
        <Grid item container direction="column" lg style={{ maxWidth: '40em' }}>
          <Grid item>
            <Typography
              variant="h4"
              align={matchesMD ? 'center' : undefined}
              gutterBottom
            >
              Technology
            </Typography>
          </Grid>
          <Grid item>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              The rise of computers, and subsequently the Internet, has
              completely altered every aspect of human life. This has increased
              our comfort, broadened our connections, and reshaped how we view
              the world.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              What once was confined to huge rooms and teams of engineers now
              resides in every single one of our hands. Harnessing this
              unlimited potential by using it to solve problems and better lives
              is at the heart of everything we do.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              We want to help businesses capitalized on the latest and greatest
              technology. The best way to predict the future is to be the one
              building if, and we want to help guide the world into this next
              chapter of technological expansion, exploration, and innovation.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              By holding ourselves to rigorous standards and pristine quality,
              we can ensure you have the absolute best tools necessary to thrive
              in this new frontier.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              We see a future where every individuals has personalized software
              custom tailored to their lifestyle, culture, and interests,
              helping them overcome life's obstacles. Each project is a step
              towards that goal.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              By holding ourselves to rigorous standards and pristine quality,
              we can ensure you have the absolute best tools necessary to thrive
              in this new frontier.
            </Typography>
            <Typography
              variant="body1"
              align={matchesMD ? 'center' : undefined}
              paragraph
            >
              We see a future where every individuals has personalized software
              custom tailored to their lifestyle, culture, and interests,
              helping them overcome life's obstacles. Each project is a step
              towards that goal.
            </Typography>
          </Grid>
        </Grid>
        <Grid item container justify={matchesMD ? 'center' : 'flex-end'} lg>
          <Lottie
            options={defaultOptions}
            isStopped={true}
            style={{ maxWidth: '40em', margin: 0 }}
          />
        </Grid>
      </Grid>
      <Grid
        item
        container
        direction="row"
        justify="center"
        className={classes.rowContainer}
      >
        <Grid item>
          <Typography variant="h4" gutterBottom>
            Process
          </Typography>
        </Grid>
      </Grid>
      <ProcessItem
        bgColor="#B3B3B3"
        headerText="Consultation"
        icon={consultation}
        iconStyle={{ maxWidth: 700 }}
        altText="handshake image"
      />
      <ProcessItem
        bgColor="#FF7373"
        headerText="Mockup"
        icon={mockup}
        iconStyle={{ maxWidth: 1000 }}
        altText="basic website design outline"
      />
      <ProcessItem
        bgColor="#39D54A"
        headerText="Review"
        icon={review}
        iconStyle={{ maxWidth: 700 }}
        altText="magnifying glass"
      />
      <ProcessItem
        bgColor="#A67C52"
        headerText="Design"
        icon={design}
        iconStyle={{ maxWidth: 1000 }}
        altText="paint brash leaving stroke of paing "
      />
      <ProcessItem
        bgColor="#FBB03B"
        headerText="Build"
        icon={build}
        iconStyle={{ maxWidth: matchesMD ? 700 : 1000 }}
        altText="building construction site"
      />
      <ProcessItem
        bgColor="#C1272D"
        headerText="Launch"
        icon={launch}
        iconStyle={{ maxWidth: matchesMD ? 100 : 200 }}
        altText="rocket"
      />
      <ProcessItem
        bgColor="#8E45CE"
        headerText="Maintain"
        icon={maintain}
        iconStyle={{ maxWidth: 500 }}
        altText="wrench tightening bolts"
      />
      <ProcessItem
        bgColor="#29ABE2"
        headerText="Iterate"
        icon={iterate}
        altText="falling dominoes"
      />
    </Grid>
  );
};

export default Revolution;
