import { makeStyles } from '@material-ui/core/styles';

import infoBackground from '../../assets/infoBackground.svg';

export const useStyles = makeStyles(theme => ({
  infoBackground: {
    backgroundImage: `url(${infoBackground})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '80em',
    width: '100%',
  },
  learnMoreButton: {
    ...theme.learnButton,
    fontSize: '0.9rem',
    height: 45,
    width: 145,
    color: 'white',
    borderColor: 'white',
  },
}));
