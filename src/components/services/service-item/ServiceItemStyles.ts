import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  serviceItemContainer: {
    marginTop: '12em',
    [theme.breakpoints.down('sm')]: {
      padding: '25px',
    },
  },
  specialText: {
    fontFamily: 'Pacifico',
    color: theme.palette.secondary.main,
  },
  learnButton: {
    ...theme.learnButton,
    fontSize: '0.7rem',
    height: 35,
    padding: 5,
    [theme.breakpoints.down('sm')]: {
      marginBottom: '2em',
    },
  },
  subtitle: {
    marginBottom: '1em',
  },
  icon: {
    marginLeft: '2em',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0,
    },
  },
}));
