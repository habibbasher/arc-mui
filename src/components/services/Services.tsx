import React from 'react';
import { connect, ConnectedProps } from 'react-redux';

import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import customSoftwareIcon from '../../assets/custom-software-icon.svg';
import mobileAppsIcon from '../../assets/mobileIcon.svg';
import websitesIcon from '../../assets/websiteIcon.svg';
import { useStyles } from './ServicesStyles';
import ServiceItem from './service-item/ServiceItem';

import {
  onSetValue,
  onSetSelectedIndex,
} from '../../redux/header/header.actions';

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
  setSelectedIdx: (payload: any) => dispatch(onSetSelectedIndex(payload)),
});

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const Services: React.FC<Props> = ({ setValue, setSelectedIdx }) => {
  const classes = useStyles();
  const theme = useTheme();
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'));
  const isBreak = matchesSM ? null : <br key={'ios-android-app-development'} />;

  const handleClick = (value: number, index: number) => {
    setValue(value);
    setSelectedIdx(index);
  };

  return (
    <>
      <ServiceItem
        wrapperJustify={matchesSM ? 'center' : undefined}
        itemTextWrapperStyle={{
          marginLeft: matchesSM ? 0 : '5em',
          textAlign: matchesSM ? 'center' : undefined,
        }}
        heading="Custom Software Development"
        subtitle1="Save Energy. Save Time. Save Money."
        subtitle2={[
          'Complete digital solutions, from investigation to ',
          <span
            key={'custom-software-development'}
            className={classes.specialText}
          >
            celebration
          </span>,
        ]}
        serviceIcon={customSoftwareIcon}
        serviceIconText="Custom software icon"
        linkTo="/mobileapps"
        clickHandler={() => handleClick(1, 1)}
      />

      <ServiceItem
        wrapperJustify={matchesSM ? 'center' : 'flex-end'}
        itemTextWrapperStyle={{
          textAlign: matchesSM ? 'center' : undefined,
        }}
        heading="IOS/Android App Development"
        subtitle1="Extend Functionalities. Extend Access. Increase Engagement."
        subtitle2={[
          'Integrate your web experience or create a standalone app ',
          isBreak,
          'with either mobile platform',
        ]}
        serviceIcon={mobileAppsIcon}
        serviceIconText="IOS/Android app icon"
        imgWrapper={{ marginRight: matchesSM ? 0 : '5em' }}
        linkTo="/customsoftware"
        clickHandler={() => handleClick(1, 2)}
      />

      <ServiceItem
        wrapperJustify={matchesSM ? 'center' : undefined}
        itemTextWrapperStyle={{
          marginLeft: matchesSM ? 0 : '5em',
          textAlign: matchesSM ? 'center' : undefined,
        }}
        heading="Website Development"
        subtitle1="Reach More. Discover More. Sell More."
        subtitle2="Organized for Search Engines, built for speed."
        serviceIcon={websitesIcon}
        serviceIconText="Website icon"
        linkTo="/websites"
        clickHandler={() => handleClick(1, 3)}
      />
    </>
  );
};

export default connector(Services);
