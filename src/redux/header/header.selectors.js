import { createSelector } from 'reselect';

const selectHeader = state => state.header;

export const selectValue = createSelector(
  [selectHeader],
  header => header.value
);

export const selectIndex = createSelector(
  [selectHeader],
  header => header.selectedIndex
);
