import React from 'react';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';

export const ElevationScroll = ({ children }: any) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
};
