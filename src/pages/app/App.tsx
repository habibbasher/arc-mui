import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Header from '../../components/ui/header/Header';
import Footer from '../../components/ui/footer/Footer';
import Home from '../home/Home';
import Services from '../services/Services';
import CustomSoftware from '../custom-software/CustomSoftware';
import MobileApps from '../mobile-apps/MobileApps';
import Websites from '../websites/Websites';
import Revolution from '../revolution/Revolution';
import Contact from '../../components/contact/Contact';

const App: React.FC = () => {
  console.log('App rendered');

  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/" render={() => <Home />} />
        <Route exact path="/services" render={() => <Services />} />
        <Route exact path="/customsoftware" render={() => <CustomSoftware />} />
        <Route exact path="/mobileapps" render={() => <MobileApps />} />
        <Route exact path="/websites" render={() => <Websites />} />
        <Route exact path="/revolution" render={() => <Revolution />} />
        <Route exact path="/about" component={() => <div>About</div>} />
        <Route exact path="/contact" render={() => <Contact />} />
        <Route exact path="/estimate" component={() => <div>Estimate</div>} />
      </Switch>
      <Footer />
    </>
  );
};

export default App;
