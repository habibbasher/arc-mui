import React from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { useTheme } from '@material-ui/core/styles';

import { useStyles } from './RevolutionStyles';
import ArrowButton from '../ui/arrow-button/ArrowButton';
import { onSetValue } from '../../redux/header/header.actions';

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
});

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const Revolution: React.FC<Props> = ({ setValue }) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Grid item>
      <Grid
        container
        alignItems="center"
        justify="center"
        style={{ height: '100em', marginTop: '12em' }}
      >
        <Card className={classes.revolutionCard}>
          <CardContent>
            <Grid container direction="column" style={{ textAlign: 'center' }}>
              <Grid item>
                <Typography variant="h3">The Revolution</Typography>
              </Grid>
              <Grid item>
                <Typography variant="subtitle1">
                  Visionary insights coupled with cutting-edge technology is a
                  recipe for revolution
                </Typography>
                <Button
                  variant="outlined"
                  component={Link}
                  to="/revolution"
                  onClick={() => setValue(2)}
                  className={classes.learnMoreButton}
                >
                  <span style={{ marginRight: 10 }}>Learn More</span>
                  <ArrowButton
                    width={15}
                    height={15}
                    fill={theme.palette.primary.main}
                  />
                </Button>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
        <div className={classes.revolutionBackground} />
      </Grid>
    </Grid>
  );
};

export default connector(Revolution);
