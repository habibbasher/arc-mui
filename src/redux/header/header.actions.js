import { HeaderActionTypes } from './header.types';

export const onSetValue = (value = 0) => ({
  type: HeaderActionTypes.SET_VALUE,
  payload: value,
});

export const onSetSelectedIndex = (index = 0) => ({
  type: HeaderActionTypes.SET_SELECTED_ITEM,
  payload: index,
});
