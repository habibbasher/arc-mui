export interface HeaderRoutes {
  name: string;
  link: string;
  activeIdx: number;
  selectedIdx?: number;
  ariaOwns?: string | undefined;
  ariaHaspopup?: boolean | undefined;
  onMouseOver?: any;
}

export interface Props {
  value: number;
  setValue: Function;
  selectedIdx: number;
  setSelectedIdx: Function;
}
