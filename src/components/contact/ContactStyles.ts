import { makeStyles } from '@material-ui/core/styles';
import background from '../../assets/background.jpg';
import mobileBackground from '../../assets/mobileBackground.jpg';

export const useStyles = makeStyles(theme => ({
  background: {
    backgroundImage: `url(${background})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '60em',
    paddingBottom: '10em',
    [theme.breakpoints.down('md')]: {
      backgroundImage: `url(${mobileBackground})`,
    },
  },
  message: {
    border: `2px solid ${theme.palette.primary.main}`,
    marginTop: '3em',
    borderRadius: 5,
  },
  error: {
    color: '#f44336',
  },
  sendButton: {
    ...theme.estimate,
    borderRadius: 50,
    height: 45,
    width: 245,
    fontSize: '1ren',
    backgroundColor: theme.palette.secondary.main,
    '&:hover': {
      backgroundColor: theme.palette.secondary.light,
    },
    [theme.breakpoints.down('sm')]: {
      height: 40,
      width: 225,
    },
  },
  learnMoreButton: {
    ...theme.learnButton,
    fontSize: '0.7rem',
    height: 35,
    padding: 5,
    [theme.breakpoints.down('md')]: {
      marginBottom: '2em',
    },
  },
  estimateButton: {
    ...theme.estimate,
    borderRadius: 50,
    height: 50,
    width: 205,
    backgroundColor: theme.palette.secondary.main,
    fontSize: '1.5rem',
    marginRight: '5em',
    marginLeft: '2em',
    [theme.breakpoints.down('md')]: {
      marginLeft: 0,
      marginRight: 0,
    },
    '&:hover': {
      backgroundColor: theme.palette.secondary.light,
    },
  },
}));
