import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  specialText: {
    fontFamily: 'Pacifico',
    color: theme.palette.secondary.main,
  },
}));
