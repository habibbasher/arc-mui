import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import boardReducer from './board/board.reducer';
import headerReducer from './header/header.reducer';

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['board', 'header'],
};

const rootReducer = combineReducers({
  board: boardReducer,
  header: headerReducer,
});

export default persistReducer(persistConfig, rootReducer);
