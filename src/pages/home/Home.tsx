import React from 'react';
import Grid from '@material-ui/core/Grid';

import { useStyles } from './HomeStyles';
import Hero from '../../components/hero/Hero';
import Services from '../../components/services/Services';
import Revolution from '../../components/revolution/Revolution';
import Information from '../../components/information/Information';
import CallToAction from '../../components/call-to-action/CallToAction';

const Home: React.FC = () => {
  const classes = useStyles();
  console.log('Home rendered');
  return (
    <Grid container direction="column" className={classes.mainContainer}>
      <Hero />
      <Services />
      <Revolution />
      <Information />
      <CallToAction />
    </Grid>
  );
};

export default Home;
