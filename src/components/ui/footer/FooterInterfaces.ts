export interface Props {
  value?: number;
  setValue: Function;
  selectedIdx?: number;
  setSelectedIdx: Function;
}
