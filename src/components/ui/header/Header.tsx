import React, { useState, useEffect, ChangeEvent } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import MenuICon from '@material-ui/icons/Menu';

import logo from '../../../assets/logo.svg';
import { ElevationScroll } from '../../lib/wrappers';
import { useStyles } from './HeaderStyles';
import { menuOptions } from './HeaderUtils';
import { HeaderRoutes } from './HeaderInterfaces';

import {
  onSetValue,
  onSetSelectedIndex,
} from '../../../redux/header/header.actions';
import {
  selectValue,
  selectIndex,
} from '../../../redux/header/header.selectors';

const getTabProps = (index: number) => {
  return {
    id: `header-tab-${index}`,
    'aria-controls': `header-tabpanel-${index}`,
  };
};

const mapState = createStructuredSelector({
  value: selectValue,
  selectedIdx: selectIndex,
});

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
  setSelectedIdx: (payload: any) => dispatch(onSetSelectedIndex(payload)),
});

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const Header: React.FC<Props> = ({
  value,
  setValue,
  selectedIdx,
  setSelectedIdx,
}) => {
  const classes = useStyles();
  const theme = useTheme();
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  const matches = useMediaQuery(theme.breakpoints.down('md'));
  const { pathname } = useLocation();

  const [openDrawer, setOpenDrawer] = useState(false);

  const [anchorEl, setAnchorEl] = useState(null);
  const [openMenu, setOpenMenu] = useState(false);

  const handleTabChange = (e: ChangeEvent<any>, newValue: number) => {
    setValue(newValue);
  };

  const handleMenuOpen = (e: ChangeEvent<any>) => {
    setAnchorEl(e.currentTarget);
    setOpenMenu(true);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setOpenMenu(false);
  };

  const handleMenuItemClick = (e: ChangeEvent<any>, i: number) => {
    setAnchorEl(null);
    setOpenMenu(false);
    setSelectedIdx(i);
  };

  const routes: HeaderRoutes[] = [
    { name: 'Home', link: '/', activeIdx: 0 },
    {
      name: 'Services',
      link: '/services',
      activeIdx: 1,
      ariaOwns: anchorEl ? 'service-menu' : undefined,
      ariaHaspopup: anchorEl ? true : undefined,
      onMouseOver: (event: ChangeEvent<any>) => handleMenuOpen(event),
    },
    { name: 'The Revolution', link: '/revolution', activeIdx: 2 },
    { name: 'About Us', link: '/about', activeIdx: 3 },
    { name: 'Contact Us', link: '/contact', activeIdx: 4 },
  ];

  const setActiveTab = () => {
    [...menuOptions, ...routes].forEach(route => {
      switch (pathname) {
        case `${route.link}`:
          if (value !== route.activeIdx) {
            setValue(route.activeIdx);
            if (route.selectedIdx && route.selectedIdx !== selectedIdx) {
              setSelectedIdx(route.selectedIdx);
            }
          }
          break;
        case '/estimate':
          setValue(5);
          break;
        default:
          break;
      }
    });
  };

  useEffect(() => {
    setActiveTab();
  }, []);

  const tabs = (
    <>
      <Tabs
        className={classes.tabContainer}
        value={value === 5 ? false : value}
        onChange={handleTabChange}
        indicatorColor="primary"
        aria-label="header tabs"
      >
        {routes.map((route, idx) => (
          <Tab
            key={`${route.link}-${idx}`}
            className={classes.tab}
            component={Link}
            to={route.link}
            label={route.name}
            aria-owns={route.ariaOwns}
            aria-haspopup={route.ariaHaspopup}
            onMouseOver={route.onMouseOver}
            {...getTabProps(idx)}
          />
        ))}
      </Tabs>
      <Button
        variant="contained"
        color="secondary"
        className={classes.button}
        component={Link}
        to="/estimate"
        onClick={() => setValue(5)}
      >
        Free Estimate
      </Button>
      <Menu
        id="service-menu"
        anchorEl={anchorEl}
        open={openMenu}
        onClick={handleMenuClose}
        classes={{ paper: classes.menu }}
        MenuListProps={{ onMouseLeave: handleMenuClose }}
        elevation={0}
        style={{ zIndex: 1302 }}
        keepMounted
      >
        {menuOptions.map((option, idx) => (
          <MenuItem
            key={`${option.link}-${idx}`}
            classes={{ root: classes.menuItem }}
            onClick={(event: ChangeEvent<any>) => {
              handleMenuClose();
              setValue(1);
              handleMenuItemClick(event, idx);
            }}
            selected={value === 1 && idx === selectedIdx}
            component={Link}
            to={option.link}
          >
            {option.name}
          </MenuItem>
        ))}
      </Menu>
    </>
  );

  const drawer = (
    <>
      <SwipeableDrawer
        disableBackdropTransition={!iOS}
        disableDiscovery={iOS}
        open={openDrawer}
        onClose={() => setOpenDrawer(false)}
        onOpen={() => setOpenDrawer(true)}
        classes={{ paper: classes.drawer }}
      >
        <div className={classes.toolbarMargin} />
        <List disablePadding>
          {routes.map((route, idx) => (
            <ListItem
              key={idx}
              divider
              button
              onClick={() => {
                setOpenDrawer(false);
                setValue(route.activeIdx);
              }}
              component={Link}
              to={route.link}
              selected={value === route.activeIdx}
            >
              <ListItemText
                disableTypography
                className={
                  value === route.activeIdx
                    ? classes.drawerItem + ' ' + classes.drawerItemSelected
                    : classes.drawerItem
                }
              >
                {route.name}
              </ListItemText>
            </ListItem>
          ))}
          <ListItem
            divider
            button
            className={classes.drawerItemEstimate}
            onClick={() => {
              setOpenDrawer(false);
              setValue(5);
            }}
            component={Link}
            to="/estimate"
            selected={value === 5}
          >
            <ListItemText
              disableTypography
              className={
                value === 5
                  ? classes.drawerItem + ' ' + classes.drawerItemSelected
                  : classes.drawerItem
              }
            >
              Free Estimate
            </ListItemText>
          </ListItem>
        </List>
      </SwipeableDrawer>
      <IconButton
        className={classes.drawerIconContainer}
        onClick={() => setOpenDrawer(!openDrawer)}
        disableRipple
      >
        <MenuICon className={classes.drawerIcon} />
      </IconButton>
    </>
  );

  return (
    <>
      <ElevationScroll>
        <AppBar position="fixed" className={classes.appbar}>
          <Toolbar disableGutters>
            <Button
              disableRipple
              component={Link}
              to="/"
              onClick={() => setValue(0)}
              className={classes.logoContainer}
            >
              <img src={logo} className={classes.logo} alt="company logo" />
            </Button>
            {matches ? drawer : tabs}
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <div className={classes.toolbarMargin} />
    </>
  );
};

export default connector(Header);
