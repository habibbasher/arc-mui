import { HeaderActionTypes } from './header.types';

const INITIAL_STATE = {
  value: 0,
  selectedIndex: 0,
};

const onSetValue = (state, action) => {
  return {
    ...state,
    value: action.payload,
  };
};

const onSetSelectedIndex = (state, action) => {
  return {
    ...state,
    selectedIndex: action.payload,
  };
};

const headerReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HeaderActionTypes.SET_VALUE:
      return onSetValue(state, action);
    case HeaderActionTypes.SET_SELECTED_ITEM:
      return onSetSelectedIndex(state, action);
    default:
      return state;
  }
};

export default headerReducer;
