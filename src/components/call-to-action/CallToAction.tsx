import React from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import { useStyles } from './CallToActionStyles';
import ArrowButton from '../ui/arrow-button/ArrowButton';
import { onSetValue } from '../../redux/header/header.actions';

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
});

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const CallToAction: React.FC<Props> = ({ setValue }) => {
  const classes = useStyles();
  const theme = useTheme();
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'));

  return (
    <Grid item>
      <Grid
        container
        direction={matchesSM ? 'column' : 'row'}
        alignItems="center"
        justify={matchesSM ? 'center' : 'space-between'}
        className={classes.background}
      >
        <Grid
          item
          style={{
            marginLeft: matchesSM ? 0 : '5em',
            textAlign: matchesSM ? 'center' : 'inherit',
          }}
        >
          <Grid container direction="column">
            <Grid item>
              <Typography variant="h2">
                Simple Software.
                <br />
                Revolutionary
              </Typography>
              <Typography variant="subtitle2" style={{ fontSize: '1.5rem' }}>
                Take advantage of the 21t Century
              </Typography>
              <Grid item container justify={matchesSM ? 'center' : undefined}>
                <Button
                  variant="outlined"
                  component={Link}
                  to="/revolution"
                  onClick={() => setValue(2)}
                  className={classes.learnMoreButton}
                >
                  <span style={{ marginRight: 5 }}>Learn More</span>
                  <ArrowButton
                    width={10}
                    height={10}
                    fill={theme.palette.primary.main}
                  />
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            component={Link}
            to="/estimate"
            onClick={() => setValue(5)}
            className={classes.estimateButton}
          >
            Free Estimate
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default connector(CallToAction);
