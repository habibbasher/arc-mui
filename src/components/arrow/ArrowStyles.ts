import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  arrowContainer: {
    marginTop: '0.5em',
  },
}));
