import React from 'react';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';

import { useStyles } from './ArrowStyles';
import { ArrowProps } from './ArrowProps';

const Arrow: React.FC<ArrowProps> = ({
  toLink,
  clickHandler,
  arrow,
  altText,
  extraClass,
}) => {
  const classes = useStyles();
  return (
    <Hidden mdDown>
      <Grid item className={classes.arrowContainer} style={extraClass}>
        <IconButton
          style={{ backgroundColor: 'transparent' }}
          component={Link}
          to={toLink}
          onClick={() => clickHandler}
        >
          <img src={arrow} alt={altText} />
        </IconButton>
      </Grid>
    </Hidden>
  );
};

export default Arrow;
