import { createMuiTheme, ThemeOptions } from '@material-ui/core/styles';

const arcBlue = '#0B72B9';
const arcOrange = '#FFBA60';
const arcGray = '#868686';

declare module '@material-ui/core/styles/createMuiTheme' {
  interface Theme {
    estimate?: any;
    tab?: any;
    learnButton?: any;
  }
  interface ThemeOptions {
    estimate?: any;
    tab?: any;
    learnButton?: any;
  }
}

const createMyTheme = (options: ThemeOptions) => {
  return createMuiTheme({
    estimate: {
      fontFamily: 'Pacifico',
      fontSize: '1rem',
      textTransform: 'none',
      color: 'white',
    },
    tab: {
      fontFamily: 'Raleway',
      textTransform: 'none',
      fontSize: '1rem',
      fontWeight: 700,
    },
    learnButton: {
      borderColor: arcBlue,
      color: arcBlue,
      borderWidth: 2,
      textTransform: 'none',
      borderRadius: 50,
      fontFamily: 'Roboto',
      fontWeight: 'bold',
    },
    ...options,
  });
};

const theme = createMyTheme({
  palette: {
    common: {
      black: '#000',
      white: '#fff',
    },
    primary: {
      main: arcBlue,
    },
    secondary: {
      main: arcOrange,
    },
  },
  typography: {
    h2: {
      fontFamily: 'Raleway',
      fontSize: '2.5rem',
      fontWeight: 700,
      color: arcBlue,
      lineHeight: 1.5,
    },
    h3: {
      fontFamily: 'Pacifico',
      fontSize: '2.5rem',
      color: arcBlue,
    },
    h4: {
      fontFamily: 'Raleway',
      fontSize: '1.75rem',
      fontWeight: 700,
      color: arcBlue,
    },
    subtitle1: {
      fontSize: '1.25rem',
      fontWeight: 300,
      color: arcGray,
    },
    subtitle2: {
      fontSize: '1.25rem',
      fontWeight: 300,
      color: 'white',
    },
    body1: {
      fontSize: '1.25rem',
      fontWeight: 300,
      color: arcGray,
    },
  },
  overrides: {
    MuiInputLabel: {
      root: {
        color: arcBlue,
        fontSize: '1rem',
      },
    },
    MuiInput: {
      root: {
        color: arcGray,
        fontWeight: 300,
      },
      underline: {
        '&:before': {
          borderBottom: `2px solid ${arcBlue}`,
        },
        '&:hover:not($disabled):not($error):not($focused):before': {
          borderBottom: `2px solid ${arcBlue}`,
        },
      },
    },
  },
});

export default theme;
