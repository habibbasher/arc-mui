import { makeStyles } from '@material-ui/core/styles';

import revolutionBackground from '../../assets/repeatingBackground.svg';

export const useStyles = makeStyles(theme => ({
  learnMoreButton: {
    ...theme.learnButton,
    fontSize: '0.9rem',
    height: 45,
    width: 145,
  },
  revolutionBackground: {
    backgroundImage: `url(${revolutionBackground})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    height: '100%',
    width: '100%',
  },
  revolutionCard: {
    position: 'absolute',
    boxShadow: theme.shadows[10],
    borderRadius: '15px',
    padding: '10em',
    [theme.breakpoints.down('sm')]: {
      paddingTop: '8em',
      paddingBottom: '8em',
      paddingLeft: 0,
      paddingRight: 0,
      borderRadius: 0,
      width: '100%',
    },
  },
}));
