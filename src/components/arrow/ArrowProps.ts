export interface ArrowProps {
  toLink: string;
  clickHandler: Function;
  arrow: any;
  altText: string;
  extraClass?: object;
}
