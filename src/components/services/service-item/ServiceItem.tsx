import React from 'react';
import { Link } from 'react-router-dom';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core/styles';

import { Props } from './ServiceItemInterface';
import { useStyles } from './ServiceItemStyles';
import ArrowButton from '../../ui/arrow-button/ArrowButton';

const ServiceItem: React.FC<Props> = ({
  wrapperJustify,
  wrapperStyles,
  itemTextWrapperStyle,
  heading,
  subtitle1,
  subtitle2,
  serviceIcon,
  serviceIconText,
  imgWrapper,
  imgStyle,
  linkTo,
  clickHandler,
}) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Grid item>
      <Grid
        container
        direction="row"
        justify={wrapperJustify}
        className={classes.serviceItemContainer}
        style={wrapperStyles}
      >
        <Grid item style={itemTextWrapperStyle}>
          <Typography variant="h4">{heading}</Typography>
          <Typography variant="subtitle1" className={classes.subtitle}>
            {subtitle1}
          </Typography>
          <Typography variant="subtitle1">{subtitle2}</Typography>
          <Button
            variant="outlined"
            component={Link}
            to={linkTo}
            onClick={() => clickHandler()}
            className={classes.learnButton}
          >
            <span style={{ marginRight: '5px' }}>Learn More</span>
            <ArrowButton
              width={10}
              height={10}
              fill={theme.palette.primary.main}
            />
          </Button>
        </Grid>
        <Grid item style={imgWrapper}>
          <img
            style={imgStyle}
            src={serviceIcon}
            alt={serviceIconText}
            className={classes.icon}
          />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ServiceItem;
