export interface Props {
  wrapperJustify?: any;
  wrapperStyles?: any;
  itemTextWrapperStyle?: any;
  heading: string;
  subtitle1: string;
  subtitle2: any;
  serviceIcon: any;
  serviceIconText: string;
  imgWrapper?: any;
  imgStyle?: any;
  linkTo: string;
  clickHandler: Function;
}
