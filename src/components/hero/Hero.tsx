import React from 'react';
import { Link } from 'react-router-dom';
import Lottie from 'react-lottie';
import { connect, ConnectedProps } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core/styles';

import animationData from '../../animations/landinganimation/data';
import { useStyles } from './HeroStyles';
import ArrowButton from '../ui/arrow-button/ArrowButton';
import { onSetValue } from '../../redux/header/header.actions';

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
});

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const Hero: React.FC<Props> = ({ setValue }) => {
  const classes = useStyles();
  const theme = useTheme();

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

  return (
    <Grid item>
      <Grid container justify="flex-end" alignItems="center" direction="row">
        <Grid sm item className={classes.heroTextContainer}>
          <Typography variant="h2" align="center">
            Brining West Coast Technology
            <br /> to the Midwest
          </Typography>
          <Grid container justify="center" className={classes.buttonContainer}>
            <Grid item>
              <Button
                variant="contained"
                component={Link}
                to="/estimate"
                onClick={() => setValue(5)}
                className={classes.estimateButton}
              >
                Free Estimate
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                component={Link}
                to="/revolution"
                onClick={() => setValue(2)}
                className={classes.learnMoreButton}
              >
                <span style={{ marginRight: 10 }}>Learn More</span>
                <ArrowButton
                  width={15}
                  height={15}
                  fill={theme.palette.primary.main}
                />
              </Button>
            </Grid>
          </Grid>
        </Grid>
        <Grid sm item className={classes.animation}>
          <Lottie options={defaultOptions} height={'100%'} width={'100%'} />
        </Grid>
      </Grid>
    </Grid>
  );
};

export default connector(Hero);
