import React from 'react';
import { Link } from 'react-router-dom';
import { connect, ConnectedProps } from 'react-redux';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

import { useStyles } from './InformationStyles';
import ArrowButton from '../ui/arrow-button/ArrowButton';

import { onSetValue } from '../../redux/header/header.actions';

const mapDispatch = (dispatch: any) => ({
  setValue: (payload: any) => dispatch(onSetValue(payload)),
});

const connector = connect(null, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>;

interface Props extends PropsFromRedux {}

const Information: React.FC<Props> = ({ setValue }) => {
  const classes = useStyles();
  const theme = useTheme();
  const matchesSM = useMediaQuery(theme.breakpoints.down('sm'));
  const matchesXS = useMediaQuery(theme.breakpoints.down('xs'));

  return (
    <Grid item>
      <Grid
        container
        direction="row"
        alignItems="center"
        className={classes.infoBackground}
      >
        <Grid
          item
          container
          style={{
            textAlign: matchesXS ? 'center' : 'inherit',
          }}
          direction={matchesXS ? 'column' : 'row'}
        >
          <Grid
            item
            sm
            style={{ marginLeft: matchesXS ? 0 : matchesSM ? '2em' : '5em' }}
          >
            <Grid
              container
              direction="column"
              style={{ marginBottom: matchesXS ? '10em' : 10 }}
            >
              <Typography variant="h2" style={{ color: 'white' }}>
                About Us
              </Typography>
              <Typography variant="subtitle2">Let's get personal.</Typography>
              <Grid item>
                <Button
                  variant="outlined"
                  component={Link}
                  to="/about"
                  onClick={() => setValue(3)}
                  className={classes.learnMoreButton}
                >
                  <span style={{ marginRight: 10 }}>Learn More</span>
                  <ArrowButton width={10} height={10} fill="white" />
                </Button>
              </Grid>
            </Grid>
          </Grid>
          <Grid
            item
            sm
            style={{
              marginRight: matchesXS ? 0 : matchesSM ? '2em' : '5em',
              textAlign: matchesXS ? 'center' : 'right',
            }}
          >
            <Grid container direction="column">
              <Typography variant="h2" style={{ color: 'white' }}>
                Contact Us
              </Typography>
              <Typography variant="subtitle2">Say hello!</Typography>
              <Grid item>
                <Button
                  variant="outlined"
                  component={Link}
                  to="/contact"
                  onClick={() => setValue(4)}
                  className={classes.learnMoreButton}
                >
                  <span style={{ marginRight: 10 }}>Learn More</span>
                  <ArrowButton width={10} height={10} fill="white" />
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default connector(Information);
