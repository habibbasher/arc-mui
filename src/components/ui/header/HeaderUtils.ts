import { HeaderRoutes } from './HeaderInterfaces';

export const menuOptions: HeaderRoutes[] = [
  { name: 'Services', link: '/services', activeIdx: 1, selectedIdx: 0 },
  {
    name: 'Custom Software Development',
    link: '/customsoftware',
    activeIdx: 1,
    selectedIdx: 1,
  },
  {
    name: 'IOS/Android App Development',
    link: '/mobileapps',
    activeIdx: 1,
    selectedIdx: 2,
  },
  {
    name: 'Website Development',
    link: '/websites',
    activeIdx: 1,
    selectedIdx: 3,
  },
];
