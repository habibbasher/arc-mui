export interface Props {
  styleStr?: string;
  width: number;
  height: number;
  fill: string;
}
