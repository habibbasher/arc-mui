import * as yup from 'yup';

export const CONTACT_INITIAL_STATE = {
  name: '',
  email: '',
  phone: '',
  message: '',
};

//errors by the order of the form inputs in yup as these validations run in parallel!
export const VALIDATION_SCHEMA = yup.object({
  name: yup.string().required(),
  email: yup.string().required().email(),
  phone: yup.string().required(),
  message: yup.string().required(),
});
